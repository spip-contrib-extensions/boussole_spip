<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion de la boussole courante dans la liste des boussoles pouvant être fournies par le serveur.
 *
 * @param array $boussoles Listes des boussoles et de leur plugin fournisseur.
 *
 * @return array Liste des boussoles mise à jour
 */
function bouspip_declarer_boussoles(array $boussoles) : array {
	$boussoles['spip'] = ['prefixe' => 'bouspip'];

	return $boussoles;
}

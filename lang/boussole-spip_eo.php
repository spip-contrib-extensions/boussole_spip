<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// D
	'descriptif_boussole_spip' => 'La SPIP-kompaso kunigas la aron de « oficialaj » retejoj de la SPIP galaksio. Ĝi registras pri ĉiu retejo ĝian vinjeton, ĝian nomon, ĝian devizon kaj ĝian priskribon. Do ne hezitu uzi ĝin ĉe viaj propraj retejoj por orientigi viajn vizitantojn tra la SPIP galaksio.',
	'descriptif_site_spip_blog' => 'Ĉar SPIP estas kunlaboriga projekto, spip-blog.net kolektas teknikajn artikoletojn, mem mokadojn, trolojn, diversajn anoncojn , … Pro tio ĝi bone respegulas la SPIP komunumon : unue kaj precipe multege de tenero.', # MODIF
	'descriptif_site_spip_contrib' => 'Kiel kunlaboriga retejo, contrib.spip disponigas la tuton de la exteraj kontribuaĵoj : kromprogramojn, skriptojn, filtrilojn, skeletojn, dokumentojn, trukojn kaj elturnigilojn,… el la komunumo (elŝutligoj) far la SPIP uzantoj. Ĝiaj forumoj plenumas la ligon inter konceptistoj kaj uzantoj.', # MODIF
	'descriptif_site_spip_demo' => 'Retejo de testo ĝisdatigita ĉiunokte, demo.spip.net ebligas al ĉiu provi SPIP-on en ĝia plej nova stabila versio (kun, elekteble, la redakta aŭ mastrumada statuso), per nura alklako kaj sen instali ĝin. ', # MODIF
	'descriptif_site_spip_doc' => 'CODE.SPIP.net estas dokumentiga spaco de la SPIP-programaro por siaj interfacoj je aplika programado (API), sia fontokodo, kaj kelkaj el siaj teknikaj funkciiloj.',
	'descriptif_site_spip_edgard' => 'Edgard estas nelacigebla kaj fidela kompano de SPIP tuja babilejo kie ĝi ĉiam intervenas taŭge, tenere kaj humure. El sia domo edgard.spip.net ĝi portas al IRC siajn konsilojn, respondojn kaj gajecon. Cetere, Edgard estas roboto (sed ĝi sajnigas ne konsii pri tio).', # MODIF
	'descriptif_site_spip_forum' => 'Forum.spip.net estas la retejo por interŝanĝo kaj interhelpo de la SPIP uzantoj. Tiu retejo ekzistas en deko da lingvoj kaj dispartiĝas laŭ kvar grandaj rubrikoj : instalado kaj ĝisdatigo, uzado de la privata spaco, bontenado, mastrumado, retejagordo, kreado de skeletoj.', # MODIF
	'descriptif_site_spip_irc' => 'La SPIP-komunumo, neniam dormante, dotiĝis per IRC-kanalo (senprokrasta diskutado per Interreto) malfermita al ĉiuj :
http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_net' => 'Je la dispono de la uzantoj kaj de la retejestroj, SPIP.net estas la oficiala retejo konsilata por ĉiuj geuzantoj kiuj deziras instali retejon per SPIP, kompreni la lingvaĵo de iteracioj, etikedoj kaj filtriloj, skribi kaj uzi skeletojn. Ĝi prezentas glosaron, lernilojn, konsilojn, historoj pri versioj kaj spaco de elŝutado. SPIP.net estas tradukita al pli ol dudek lingvoj.', # MODIF
	'descriptif_site_spip_plugin' => 'PLUGINS.SPIP.net volas esti kompleta aro da aldonendaj moduloj por SPIP (kromprogramoj, skeletoj, grafikaj temoj). Oni prezentas por ĉiu modulo : priskribon, aŭtoron, permesilon, nivelon de kongrueco laŭ la SPIP-versio, lastajn aldonitajn modifojn, staton de tradukado, statistikojn pri uzo, ligilojn por dokumentado kaj enŝutado.',
	'descriptif_site_spip_programmer' => 'Prefere destinita al publiko konsistanta el programistoj aŭ retejestroj kiuj jam havas konojn pri PHP, SQL, HTML, CSS kaj JavaScript, PROGRAMMER.SPIP.net prezentas la plej parto de la SPIP-funkciecoj (API-oj, superregoj, duktoj, …) tra multnombraj kodekzemploj. La retejo proponas elŝute la kompleton de sia enhavo en pdf-formato per libera permesilo cc-by-sa. PROGRAMMER.SPIP.net konsulteblas en la franca, angla kaj hispana.', # MODIF
	'descriptif_site_spip_trad' => 'La tradukanto-spaco bonvenigas ĉiujn, kiuj deziras helpi la uzant-komunumo de SPIP partoprenante al la traduko de SPIP mem kaj de ĝiaj diversaj kontribuaĵoj.', # MODIF

	// N
	'nom_boussole_spip' => 'SPIP-Orientigilo',
	'nom_groupe_spip_actualite' => 'Aktualaĵo',
	'nom_groupe_spip_aide' => 'Reciproka helpo',
	'nom_groupe_spip_decouverte' => 'Ekmalkovro',
	'nom_groupe_spip_extension' => 'Kontribuoj', # MODIF
	'nom_groupe_spip_reference' => 'Dokumentado',
	'nom_site_spip_blog' => 'SPIP Blogo', # MODIF
	'nom_site_spip_contrib' => 'SPIP-Kontribuo', # MODIF
	'nom_site_spip_demo' => 'SPIP Elmontrado', # MODIF
	'nom_site_spip_doc' => 'SPIP Kodo', # MODIF
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forum' => 'SPIP Forumoj',
	'nom_site_spip_irc' => 'SPIP Tuja Babilejo', # MODIF
	'nom_site_spip_net' => 'SPIP.net', # MODIF
	'nom_site_spip_plugin' => 'Kromprogramoj SPIP', # MODIF
	'nom_site_spip_programmer' => 'SPIP Programi', # MODIF
	'nom_site_spip_test' => 'SPIP Testi', # MODIF
	'nom_site_spip_trad' => 'SPIP Traduki', # MODIF
	'nom_site_spip_video' => 'SPIP Mediateko', # MODIF

	// S
	'slogan_groupe_spip_actualite' => 'SPIP Aktualaĵoj',
	'slogan_groupe_spip_aide' => 'Helpo kaj interŝanĝoj ĉirkaŭ SPIP',
	'slogan_groupe_spip_decouverte' => 'Ekmalkovro de SPIP',
	'slogan_groupe_spip_extension' => 'Etendigoj kaj kontribuoj al SPIP',
	'slogan_groupe_spip_reference' => 'Referencoj SPIP',
	'slogan_site_spip_blog' => 'Libera programaro kaj tenereco',
	'slogan_site_spip_edgard' => '(Ro)boto pasas kaj ek !',
	'slogan_site_spip_net' => 'La oficiala dokumentado kaj la SPIP elŝute',
	'slogan_site_spip_video' => 'La mediateko de SPIP',
];

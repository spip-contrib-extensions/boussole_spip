<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// D
	'descriptif_boussole_spip' => 'Kompas SPIPu zoskupuje všetky "oficiálne" stránky galaxie SPIPu. Pri každej stránke uvádza jej logo, názov, slogan a jej opis. Pokojne ju môžete použiť na vlastných stránkach na nasmerovanie svojich návštevníkov do galaxie SPIPu.',
	'descriptif_site_spip_blog' => 'Keďže SPIP je kolektívny projekt, SPIP-BLOG.net  sústreďuje technické poznámky, staršie verzie, nálady, chyby, rôzne oznamy, atď. Takto odráža komunitu okolo SPIPu: na prvom mieste a predovšetkým ľahkosťou. énormément de tendresse.', # MODIF
	'descriptif_site_spip_contrib' => 'Kolektívna stránka contrib.spip poskytuje všetky externé príspevky: zásuvné moduly, skripty, filtre, šablóny, dokumentáciu, tipy a triky, atď., ktoré vytvorila komunita okolo SPIPu (v podobe odkazov na sťahovanie) pre používateľov SPIPu. V diskusných fórach nájdete hypertextové odkazy vývojárov pre používateľov.', # MODIF
	'descriptif_site_spip_demo' => 'DEMO.SPIP.org je testovacia stránka, ktorá sa nanovo nastavuje každú noc, každému umožňuje otestovať najnovšiu stabilnú verziu SPIPu (s výberom funkcie redaktora alebo administrátora) jedným klikom a bez inštalácie.', # MODIF
	'descriptif_site_spip_doc' => 'CODE.SPIP.org je priestor pre dokumentáciu programu SPIP pre jeho aplikácie, zdrojový kód a jeho niektoré technické funkcie.',
	'descriptif_site_spip_edgard' => 'Edgard je verný spoločník a neúnavný IRC spoločník SPIPom, ktorý vždy pracuje múdro, s ľahkosťou a humorom. Zo svojho domova na EDGARD.SPIP.org prináša na IRC rady, odpovede a ten správny humor. Mimochodom, Edgard je robot (ale nepôsobí tak)', # MODIF
	'descriptif_site_spip_forum' => 'FORUM.SPIP.org je stránka na výmenu informácií a vzájomnú pomoc pre používateľov SPIPu. Stránka, ktorá existuje v mnohých jazykoch, má štyri hlavné rubriky: inštalácia a aktualizácia, používanie súkromnej zóny, spravovanie, riadenie a konfigurácia stránky a vytváranie šablón.', # MODIF
	'descriptif_site_spip_irc' => 'Komunita okolo SPIPu, ktorá nikdy nespí, založila kanál IRC (chat na internete v reálnom čase) otvorený pre všetkých: http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_net' => 'Stránka SPIP.net, ktorá je určená pre používateľov a webmasterov, je oficiálne odporúčaná všetkým tým, ktorí si chcú nainštalovať SPIP, rozumejú jazyku cyklov, filtrov a tagov, pre tých, ktorí programujú a používajú šablóny. Je tu terminologický slovník (resp. glosár), návody, tipy, históriu verzií a sťahovaciu zónu. SPIP.net je preložený do vyše dvadsiatich jazykov.', # MODIF
	'descriptif_site_spip_plugin' => 'Cieľom PLUGINS.SPIP.net je vytvoriť úplný priečinok doplnkových modulov  pre SPIP (zásuvné moduly, šablóny, farebné motívy). Ku každému modulu je uvedený: opis, autor, licencia, úroveň kompatibility s verziou SPIPu, naposledy vykonané zmeny, stav prekladov, štatistika používania, odkazy na dokumentáciu a stiahnutie.',
	'descriptif_site_spip_programmer' => 'Skôr určená pre vývojárov alebo webmasterov, ktorí už vedia programovať v PHP, SQL, HTML, CSS a JavaScripte,  poskytuje množstvo funkcií (aplikácie API, nadmerné zaťaženie servera, prepojenia, atď.) SPIPu s mnohými príkladmi použitia kódu. Stránka ponúka používateľom možnosť stiahnuť si celý jej obsah vo formáte pdf so slobodnou licenciou cc-by-sa. PROGRAMMER.SPIP.org je dostupná vo francúzštine, v angličtine a v španielčine.', # MODIF
	'descriptif_site_spip_trad' => 'Prekladateľská zóna privíta všetkých, ktorí chcú pomôcť používateľom SPIPu  prekladom samotného SPIPu a jeho rôznych rozšírení.', # MODIF
	'descriptif_site_spip_video' => 'Stránka MEDIAS.SPIP rozdeľuje videá vytvorené pre SPIP alebo o SPIPe. Každý používateľ môže prispieť videami, návodmi, materiálmi na konferencie, tréningové kurzy, a pod. Jedinou podmienkou je, že na materiál sa nesmú vzťahovať autorské práva, aby si materiál mohol každý bezplatne pozrieť a používať.', # MODIF

	// N
	'nom_boussole_spip' => 'Kompas SPIPu',
	'nom_groupe_spip_actualite' => 'Novinky',
	'nom_groupe_spip_aide' => 'Vzájomná pomoc',
	'nom_groupe_spip_decouverte' => 'Objavte SPIP',
	'nom_groupe_spip_extension' => 'Príspevky', # MODIF
	'nom_groupe_spip_reference' => 'Dokumentácia',
	'nom_site_spip_blog' => 'Blog o SPIPe', # MODIF
	'nom_site_spip_contrib' => 'SPIP-Contrib', # MODIF
	'nom_site_spip_demo' => 'Demo stránka so SPIPom', # MODIF
	'nom_site_spip_doc' => 'Kód SPIPu', # MODIF
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forum' => 'Diskusné fóra SPIPu',
	'nom_site_spip_irc' => 'SPIP IRC', # MODIF
	'nom_site_spip_net' => 'SPIP.net', # MODIF
	'nom_site_spip_plugin' => 'Zásuvné moduly SPIPu', # MODIF
	'nom_site_spip_plugincode' => 'Kód zásuvných modulov',
	'nom_site_spip_programmer' => 'Programovanie SPIPu', # MODIF
	'nom_site_spip_syntaxe' => 'Syntax SPIPu', # MODIF
	'nom_site_spip_test' => 'Test SPIPu', # MODIF
	'nom_site_spip_trad' => 'Preklad SPIPu', # MODIF
	'nom_site_spip_video' => 'Médiá SPIPu', # MODIF

	// S
	'slogan_boussole_spip' => 'Stratili ste sa v galaxii SPIPu!',
	'slogan_groupe_spip_actualite' => 'Novinky SPIPu',
	'slogan_groupe_spip_aide' => 'Pomoc a výmena skúseností so SPIPom',
	'slogan_groupe_spip_decouverte' => 'Objavovanie SPIPu',
	'slogan_groupe_spip_extension' => 'Rožírenia a príspevky k SPIPu',
	'slogan_groupe_spip_reference' => 'Stránky, na ktoré SPIP odkazuje',
	'slogan_site_spip_blog' => 'O slobodnom softvére a jednoduchosti ',
	'slogan_site_spip_contrib' => 'Rozhranie príspevkov k SPIP',
	'slogan_site_spip_demo' => 'Vyskúšať najnovšiu stabilnú verziu SPIPu',
	'slogan_site_spip_doc' => 'Dokumentácia ku kódu SPIPu',
	'slogan_site_spip_edgard' => 'Pomoc od robota znova a znova!',
	'slogan_site_spip_forum' => 'Diskusné fórum používateľov SPIPu', # MODIF
	'slogan_site_spip_irc' => 'Príďte a porozprávajte sa na chate SPIPu',
	'slogan_site_spip_net' => 'Oficiálna dokumentácia a stiahnutie SPIPu',
	'slogan_site_spip_plugin' => 'Priečinok so zásuvnými modulmi SPIPu',
	'slogan_site_spip_plugincode' => 'Dokumentáciu ku kódu zásuvných modulov',
	'slogan_site_spip_programmer' => 'Dokumentácia vývojárov SPIPu', # MODIF
	'slogan_site_spip_syntaxe' => 'Vyskúšajte si upravovať text v SPIPe',
	'slogan_site_spip_test' => 'Vyskúšať inštaláciu a zavedenie SPIPu na stránku',
	'slogan_site_spip_trad' => 'Prekladateľská zóna SPIPu a jeho rozšírení', # MODIF
	'slogan_site_spip_user' => 'Zoznam podpory pre používateľov SPIPu', # MODIF
	'slogan_site_spip_video' => 'Multimediálna knižnica SPIPu',
	'slogan_site_spip_zone' => 'Rozhranie vývoja príspevkov k SPIPu',
];

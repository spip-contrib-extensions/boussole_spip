<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// D
	'descriptif_boussole_spip' => 'De SPIP kompas verzamelt alle "officiële" websites van de SPIP galaxy. Van iedere website zijn het logo, de naam, de slogan en omschrijving opgeslagen. Gebruik het op jouw website om je bezoekers de SPIP Galaxy te laten ontdekken.',
	'descriptif_site_spip_blog' => 'SPIP is een collaboratief project, waarin SPIP-BLOG.net onder andere technische notities bevat. Het toont de geest van de SPIP gemeenschap: vriendelijkheid eerst!',
	'descriptif_site_spip_contrib' => 'contrib.spip.net is een collaboratieve website waarop alle externe bijdragen beschikbaar zijn: plugins, scripts, filters, skeletten, documentatie, trucks,... alles voor de gemeenschap (download koppelingen) door de mensen die SPIP gebruiken. Het forum biedt een koppeling  tussen degenen die deze bijdragen ontwikkelen en degenen die ze gebruiken.',
	'descriptif_site_spip_demo' => 'DEMO.SPIP.org is een test website die iedere nacht wordt teruggezet. Je kunt er de laatste stabiele SPIP versie testen (volgens eigen keuze het statuut van beheerder of auteur), in een klik, zonder het te hoeven installeren.',
	'descriptif_site_spip_doc' => 'DOC.SPIP.org detailleert, analyseert en becommentarieert elke functie van de SPIP broncode (naam, plaats, parameters, betekenis en toepassing).',
	'descriptif_site_spip_edgard' => 'Edgard is de trouwe onvermoeibare metgezel van SPIP IRC waar zijn kennis op vriendelijke humoristische wijze wordt gedeeld. Vanuit EDGARD.SPIP.org wordt advies, antwoord en vrolijkheid op IRC gegeven. Edgard is een robot (maar lijkt dit niet te beseffen...)',
	'descriptif_site_spip_forum' => 'FORUM.SPIP.org is waar de mensen die SPIP gebruiken elkaar vinden en helpen. Deze in 10 talen vertaalde site heeft vier hoofdrubrieken: installatie en updates, gebruik van het privé gedeelte, administratie, beheer, website configuratie, bouw van skeletten.',
	'descriptif_site_spip_irc' => 'De SPIP Gemeenschap slaapt nooit! Een IRC kanaal staat altijd open voor iedereen: http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_net' => 'Voor gebruikers, gebruiksters en webmasters is SPIP.net de officiële website voor wie een SPIP website willen installeren, om de taal (lussen, bakens en filters) te begrijpen en skeletten te maken en gebruiken. Je vindt er een woordenlijst, uitleg, adviezen en een download area voor oudere versies. SPIP.net bestaat in meer dan 20 talen.',
	'descriptif_site_spip_plugin' => 'PLUGINS.SPIP.net is een volledig overzicht van add-ons voor SPIP (plugins, skeletten, thema’s). Van iedere add-on vind je er: omschrijving, auteur, licentie, compatibiliteit met SPIP versie, laatste aanpassingen, vertaalstatus, statistieken, documentatie en download koppelingen.',
	'descriptif_site_spip_plugincode' => 'CODE.PLUGINS.SPIP.net is een documentatieruimte voor SPIP plugings en hun APIs, hun broncode en bepaalde technische functies. De site wordt automatisch gegenereerd vanaf PHPDoc in de code van deze plugins.',
	'descriptif_site_spip_programmer' => 'Eerder bedoeld voor diegenen die hun gebruik van SPIP wensen te verdiepen en reeds kennis hebben van PHP, SQL, HTML, CSS en JavaScript. PROGRAMMER.SPIP.org toont de meeste functies (APIs, overloads, pipelines,...) van SPIP met tientallen programmeervoorbeelden. De hele website kan in pdf formaat worden gedownload (free licence cc-by-sa). PROGRAMMER.SPIP.org is beschikbaar in het Frans, Engels en Spaans.',
	'descriptif_site_spip_syntaxe' => 'SPIP Syntaxe biedt een enkele pagina met het bewerkingsformulier van SPIP om alle typografische snelkoppelingen uit te proberen en onmiddellijk het resultaat te zien.',
	'descriptif_site_spip_trad' => 'De vertaalruimte is er voor wie de SPIP gemeenschap wil helpen om de SPIP software en alle bijdragen te vertalen',
	'descriptif_site_spip_video' => 'De site MEDIAS.SPIP is een toegangspunt voor de verspreiding van video’s die voor of over SPIP zijn gemaakt. Iedereen kan een bijdrage leveren door nieuw videomateriaal in te zenden over tutorials, conferenties, cursussen, enz. De enige beperking is het delen van royalty-vrije video’s, zodat iedereen ze vrij kan bekijken en gebruiken.',

	// N
	'nom_boussole_spip' => 'SPIP Galaxy',
	'nom_groupe_spip_actualite' => 'SPIP Nieuws',
	'nom_groupe_spip_aide' => 'SPIP hulp en uitwisseling',
	'nom_groupe_spip_decouverte' => 'SPIP ontdekken',
	'nom_groupe_spip_extension' => 'SPIP add-ons en bijdragen', # MODIF
	'nom_groupe_spip_reference' => 'SPIP referenties',
	'nom_site_spip_blog' => 'SPIP Blog', # MODIF
	'nom_site_spip_contrib' => 'SPIP-Contrib', # MODIF
	'nom_site_spip_demo' => 'SPIP Demo', # MODIF
	'nom_site_spip_doc' => 'SPIP Doc', # MODIF
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forum' => 'SPIP Forums',
	'nom_site_spip_irc' => 'SPIP IRC', # MODIF
	'nom_site_spip_net' => 'SPIP.net', # MODIF
	'nom_site_spip_plugin' => 'SPIP Plugins', # MODIF
	'nom_site_spip_plugincode' => 'Code van Plugins',
	'nom_site_spip_programmer' => 'Programmeren met SPIP', # MODIF
	'nom_site_spip_syntaxe' => 'SPIP Syntax', # MODIF
	'nom_site_spip_test' => 'SPIP Test', # MODIF
	'nom_site_spip_trad' => 'Translate SPIP', # MODIF
	'nom_site_spip_video' => 'SPIP Media', # MODIF

	// S
	'slogan_boussole_spip' => 'De beste koppelingen in de SPIP Galaxy!',
	'slogan_groupe_spip_actualite' => 'SPIP actualiteiten',
	'slogan_groupe_spip_aide' => 'Hulp en uitwisseling rond SPIP',
	'slogan_groupe_spip_decouverte' => 'SPIP ontdekken',
	'slogan_groupe_spip_extension' => 'Extensies en bijdragen aan SPIP',
	'slogan_groupe_spip_reference' => 'SPIP referenties',
	'slogan_site_spip_blog' => 'Gratis software en vriendelijkheid',
	'slogan_site_spip_contrib' => 'SPIP contributies',
	'slogan_site_spip_demo' => 'Test de meest recente stabiele versie van SPIP',
	'slogan_site_spip_doc' => 'Referentie van technische codering van SPIP',
	'slogan_site_spip_edgard' => 'Let it bot !',
	'slogan_site_spip_forum' => 'SPIP’s gemeenschapsforum',
	'slogan_site_spip_irc' => 'Welkom in de SPIP chat room',
	'slogan_site_spip_net' => 'Officiële SPIP documentatie en download ',
	'slogan_site_spip_plugin' => 'SPIP Plugin Directory',
	'slogan_site_spip_plugincode' => 'De documentatie van de code van plugins',
	'slogan_site_spip_programmer' => 'Documentatie voor ontwikkeling met SPIP-software',
	'slogan_site_spip_syntaxe' => 'Test het tekstbewerken in SPIP',
	'slogan_site_spip_test' => 'Test de configuratie en implementatie van een SPIP site',
	'slogan_site_spip_trad' => 'Vertaalruimte voor SPIP en de bijdragen',
	'slogan_site_spip_user' => 'Hulplijst van de SPIP-gemeenschap',
	'slogan_site_spip_video' => 'SPIP media bibliotheek',
	'slogan_site_spip_zone' => 'Gebied voor ontwikkeling van bijdragen voor SPIP',
];

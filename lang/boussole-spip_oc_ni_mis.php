<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// D
	'descriptif_boussole_spip' => 'La boussola SPIP raduna dei sit « officiels » de la galassìa SPIP. Definisse per cada sit, lou siéu logou, lou siéu noum, lou siéu eslougan e la siéu descricioun. Noun esitàs pi da l’utilisà en lu vouòstre propri sit da agulhà lu vouòstre visitaire en la galassìa SPIP.',
	'descriptif_site_spip_blog' => 'Perqué SPIP es un prouget coulabouratiéu, SPIP-BLOG.net  raduna bilhet tècnicou, autoironìa, imour, troll, anounci divers,... Per acò es ben lou reflès de la coumunità SPIP : en premié e subretout touplen de tendressa.', # MODIF
	'descriptif_site_spip_contrib' => 'Sit coulabouratiéu, contrib.spip mete a dispousicioun touti li countribucioun estèrni : plugin, escrit, filtre, esquelètrou, doucumentacioun, counsèu e filèta,... fournit a la coumunità (estac de telecargamen) dai utilisaire de SPIP. Lu siéu fòrou acherta l’estac tra lu desfouloupaire e lu utilisaire.', # MODIF
	'descriptif_site_spip_demo' => 'Sit de test reinicialisat cada nuèch, DEMO.SPIP.net permete a cadun de testà SPIP en la siéu darrièra versioun stable (embé l’estatut de redatour o d’aministratour), en un clic e sensa deure l’instalà.', # MODIF
	'descriptif_site_spip_doc' => 'CODE.SPIP.net es un espaci de doucumentacioun dóu lougiciau SPIP per li siéu API, lou siéu code sourgent, e quauqu’uni dei siéu founciounamen tècnicou.',
	'descriptif_site_spip_edgard' => 'Edgard es lou coumpàgnou fedel e infatigable de SPIP IRC doun interven toujou estoble, embé tendressa e umour. Es da la siéu maioun EDGARD.SPIP.net que pouòrta soubre IRC counsèu, respouòsta e bouòna imour. Achessouriamen, Edgard es un robot (mà noun sembla lou saupre...)', # MODIF
	'descriptif_site_spip_forum' => 'FORUM.SPIP.net es lou sit d’escambia e d’entradjuda tra lu utilisaire de SPIP. Lou sit, qu’esista en un desenau de lenga, s’ourganisa autour de quatre grani rùbrica : instalacioun e messa a jou, utilisacioun de l’espaci privat, aministracioun, gestioun, counfiguracioun dóu sit, creacioun d’esquelètrou', # MODIF
	'descriptif_site_spip_irc' => 'La coumunità SPIP, que noun duerme, a un canal IRC (discussioun instantànea per internet) dubert en toui : http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_net' => 'Per lu utilisaire e lu webmèstre, SPIP.net es lou sit ouficial racoumandat en toui aquelu que vouòlon instalà un sit embé SPIP, n’en capì lou lengage dei blouca, balisa e filtre, escriéure e utilisà les esquelètrou. presenta gloussari, tutorial, consèu, isòrica dei versioun e espaci de telecargamen. SPIP.net es traduch en mai de vint lenga.', # MODIF
	'descriptif_site_spip_plugin' => 'PLUGINS.SPIP.net vòu estre l’anuari coumplet dei mòdulou coumplementari per SPIP (plugin, esquelètrou, tèma). Soun presentat per cada mòdulou : descritiéu, autour, lichença, nivèu de coumpatibilità per versioun de SPIP, darrieri moudificacioun apourtadi, estat dei traducioun, estatìstica d’utilisacioun, estac de doucumentacioun e de telecargamen.',
	'descriptif_site_spip_plugincode' => 'CODE.PLUGINS.SPIP.net es un espaci de doucumentacioun dei plugin SPIP per li siéu API, lou siéu code sougent, e certen de lu siéu founciounamen tècnicou. Aquestou sit es generat autoumaticamen a partì dóu PHPDoc enclus en lou code dei plugin.',
	'descriptif_site_spip_programmer' => 'Mai destinat ai desfouloupaire o webmèstre que àugon jà dei counouissença en PHP, SQL, HTML, CSS et JavaScript, PROGRAMMER.SPIP.net presenta la plupart dei founciounalità (APIs, soubrescrich, pipeline,...) de SPIP embé touplen d’isemple de code. Lou sit ofra en telecargamen l’integralità dóu siéu countegut au fourmat pdf souta lichença lìbera cc-by-sa. PROGRAMMER.SPIP.net es counsultable en francès, inglès e espagnòu.', # MODIF
	'descriptif_site_spip_syntaxe' => 'SPIP Sintassa proupausa una pàgina souleta embé lou fourmulari d’edicioun de SPIP en achès libre per prouvà toui lu escourcha tipougraficou e visualisà immediatamen lou rendut.',
	'descriptif_site_spip_trad' => 'L’espaci dei tradutour aculhe toui aquelu que vouòlon ajustà la coumunità dei utilisaire de SPIP en participant au travai de traducioun de SPIP èu-meme e dei siéu diversi countribucioun.', # MODIF
	'descriptif_site_spip_video' => 'Lou sit MEDIAS.SPIP es un pounch d’intrada per la difusioun dei video  realisadi per o autour de SPIP. Cada utilisaire de SPIP pòu apourtà la siéu countribucioun en proupausant de suport video nouvèu per dei tutorial, dei counferença, dei fourmacioun... Lou soulet coustregnimen es de coumpartì dei video lìberi de drech per fin que cadun pouòsque li counsultà e li utilisà liberamen.', # MODIF

	// N
	'nom_boussole_spip' => 'Boussola SPIP',
	'nom_groupe_spip_actualite' => 'Atualità',
	'nom_groupe_spip_aide' => 'Entrajuda',
	'nom_groupe_spip_decouverte' => 'Descuberta',
	'nom_groupe_spip_extension' => 'Countribucioun', # MODIF
	'nom_groupe_spip_reference' => 'Doucumentacioun',
	'nom_site_spip_blog' => 'SPIP Blog', # MODIF
	'nom_site_spip_contrib' => 'SPIP-Contrib', # MODIF
	'nom_site_spip_demo' => 'SPIP Demo', # MODIF
	'nom_site_spip_doc' => 'SPIP Code', # MODIF
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forum' => 'SPIP Fòrou',
	'nom_site_spip_irc' => 'SPIP IRC', # MODIF
	'nom_site_spip_net' => 'SPIP.net', # MODIF
	'nom_site_spip_plugin' => 'Plugin SPIP', # MODIF
	'nom_site_spip_plugincode' => 'Code dei Plugin',
	'nom_site_spip_programmer' => 'Prougramà SPIP', # MODIF
	'nom_site_spip_syntaxe' => 'SPIP Sintassa', # MODIF
	'nom_site_spip_test' => 'SPIP Test', # MODIF
	'nom_site_spip_trad' => 'Tradurre SPIP', # MODIF
	'nom_site_spip_video' => 'Media SPIP', # MODIF

	// S
	'slogan_boussole_spip' => 'Perdut en la galassìa SPIP ?',
	'slogan_groupe_spip_actualite' => 'Atualità de SPIP',
	'slogan_groupe_spip_aide' => 'Ajuda e cambi autour de SPIP',
	'slogan_groupe_spip_decouverte' => 'A la descuberta de SPIP',
	'slogan_groupe_spip_extension' => 'Estencioun e countribucioun a SPIP',
	'slogan_groupe_spip_reference' => 'Referença SPIP',
	'slogan_site_spip_blog' => 'Dóu lougiciau lìberou e de la tendressa',
	'slogan_site_spip_contrib' => 'L’espaci dei countribucioun a SPIP',
	'slogan_site_spip_demo' => 'Prouvà la darrièra versioun stable de SPIP',
	'slogan_site_spip_doc' => 'La doucumentacioun dóu code de SPIP',
	'slogan_site_spip_edgard' => 'Un còu de bot e vaga !',
	'slogan_site_spip_forum' => 'Lou fòrou dei utilisaire de SPIP', # MODIF
	'slogan_site_spip_irc' => 'Venès charà soubre lou chat de SPIP',
	'slogan_site_spip_net' => 'La doucumentacioun ouficiala e telecargamen de SPIP',
	'slogan_site_spip_plugin' => 'L’anuari dei plugin SPIP',
	'slogan_site_spip_plugincode' => 'La doucumentacioun dóu code dei plugin',
	'slogan_site_spip_programmer' => 'La doucumentacioun dei desfouloupaire SPIP', # MODIF
	'slogan_site_spip_syntaxe' => 'Prouvà l’édicioun de tèstou en SPIP',
	'slogan_site_spip_test' => 'Prouvà l’instalacioun e la messa en obra d’un sit SPIP',
	'slogan_site_spip_trad' => 'L’espaci dei tradutour de SPIP e de li siéu countribucioun', # MODIF
	'slogan_site_spip_user' => 'La lista d’entrajuda dei utilisaire de SPIP', # MODIF
	'slogan_site_spip_video' => 'La mediatèca de SPIP',
	'slogan_site_spip_zone' => 'L’espaci de desfouloupamen dei countribucioun a SPIP',
];

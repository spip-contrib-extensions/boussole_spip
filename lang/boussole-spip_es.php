<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// D
	'descriptif_boussole_spip' => 'La brújula SPIP reagrupa el conjunto de sitios «oficiales» de la galaxia SPIP. Define para cada sitio su logotipo, su nombre, su eslogan y su descripción. No dude pues en utilizar sus propios sitios para dirigir a sus visitantes a la galaxia SPIP.',
	'descriptif_site_spip_blog' => 'Porque SPIP es un proyecto colaborativo, SPIP-BLOG.net concentra apuntes técnicos, risas de uno mismo, humores, trolls, anuncios diversos... He aquí el reflejo de la comunidad SPIP: ante todo, mucha ternura. ', # MODIF
	'descriptif_site_spip_contrib' => 'Sitio colaborativo, SPIP-CONTRIB.net pone a disposición el conjunto de las contribuciones externas: plugins, scripts, filtros, esqueletos, documentaciones, consejos y trucos... servidos a la comunidad (enlaces de descarga) por los usuarios de SPIP. Sus foros garantizan el vínculo entre desarrolladores y usuarios. ', # MODIF
	'descriptif_site_spip_demo' => 'Sitio de testeo reiniciado cada noche, DEMO.SPIP.net permite a todo el mundo probar SPIP en su última versión estable (con, según elección, el estatus de redactor o administrador), en un click y sin tener que instalarla.', # MODIF
	'descriptif_site_spip_doc' => 'CODE.SPIP.net es un espacio de documentación del software SPIP para sus API, su código fuente, y ciertas de sus funciones técnicas.',
	'descriptif_site_spip_edgard' => 'Edgard es el compañero fiel e infatigable de SPIP IRC donde siempre interviene en el momento oportuno, con ternura y humor. Es en su casa EDGARD.SPIP.net desde donde aporta a través de IRC consejos, respuestas y buen humor. Por otra parte, Edgard es un robot (sin embargo, él no da en absoluto la impresión de saberlo...)', # MODIF
	'descriptif_site_spip_forum' => 'FORUM.SPIP.net es el sitio de intercambio y ayuda entre los usuarios de SPIP. El sitio, que está disponible en una docena de idiomas, se organiza en torno a cuatro grandes apartados: instalación y actualización; uso del espacio privado; administración, gestión, configuración del sitio; creación de esqueletos.', # MODIF
	'descriptif_site_spip_irc' => 'La comunidad SPIP, que no duerme nunca, está dotada de un canal IRC (discusión instantánea por Internet) abierta a todos: http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_net' => 'Al servicio de usuarios y webmasters, SPIP.net es el sitio oficial recomendado a quienes desean instalar un sitio bajo SPIP, entendiendo el lenguaje de bucles, etiquetas y filtros, escritura y uso de esqueletos. Ofrece glosario, tutoriales, consejos, historial de las versiones y espacio de descarga. SPIP.net está traducido a más de veinte idiomas.', # MODIF
	'descriptif_site_spip_plugin' => 'PLUGINS.SPIP.net quiere ser el anuario completo de los módulos complementarios para SPIP (plugins, esqueletos, temas). Para cada módulo, se presenta: descripción, autor, licencia, nivel de compatibilidad por versión de SPIP, últimas modificaciones aportadas, estado de las traducciones, estadística de uso, enlaces de documentación y de descarga. ',
	'descriptif_site_spip_plugincode' => 'CODE.PLUGINS.SPIP.net es un espacio de documentación de los plugins SPIP para sus APIs, su código fuente, y ciertos funcionamientos técnicos. Este sitio es generados automáticamente a partir del PHPDoc incluido en el código de los plugins.',
	'descriptif_site_spip_programmer' => 'Especialmente destinado a un público de desarrolladores o webmasters teniendo ya conocimientos de PHP, SQL, HTML, CSS y JavaScript, PROGRAMMER.SPIP.net presenta la mayor parte de las funcionalidades (APIs, sobrecargas, segmentaciones...) de SPIP mediante numerosos ejemplos de código. El sitio ofrece como descarga la integridad de su contenido en formato pdf bajo licencia libre cc-by-sa. PROGRAMMER.SPIP.net puede consultarse en francés, inglés y español. ', # MODIF
	'descriptif_site_spip_syntaxe' => 'SPIP Sintaxis propone una sola página con el formulario de edición de SPIP en libre acceso para probar los atajos tipográficos y visualizar inmediatamente el resultado.',
	'descriptif_site_spip_trad' => 'El espacio de los traductores acoge a todos aquéllos que desean ayudar a la comunidad de usuarios de SPIP participando en el trabajo de traducción de SPIP en sí mismo y de sus diversas contribuciones.', # MODIF
	'descriptif_site_spip_video' => 'El sitio MEDIAS.SPIP es un punto de entrada para la difusión de vídeos realizados para o en torno a SPIP. Todo usuario de SPIP puede aportar su contribución proponiendo nuevos soportes de vídeos relatando tutoriales, conferencias, formaciones... La única debilidad radica en compartir vídeos libres de derechos para que cada uno pueda consultarlos y explotarlos libremente.', # MODIF

	// N
	'nom_boussole_spip' => 'Brújula SPIP',
	'nom_groupe_spip_actualite' => 'Actualidad',
	'nom_groupe_spip_aide' => 'Ayuda',
	'nom_groupe_spip_decouverte' => 'Descubrimiento',
	'nom_groupe_spip_extension' => 'Contribuciones', # MODIF
	'nom_groupe_spip_reference' => 'Documentación',
	'nom_site_spip_blog' => 'SPIP Blog', # MODIF
	'nom_site_spip_contrib' => 'SPIP-Contrib', # MODIF
	'nom_site_spip_demo' => 'SPIP Demo', # MODIF
	'nom_site_spip_doc' => 'SPIP Código', # MODIF
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forum' => 'SPIP Foros',
	'nom_site_spip_irc' => 'SPIP IRC', # MODIF
	'nom_site_spip_net' => 'SPIP.net', # MODIF
	'nom_site_spip_plugin' => 'Plugins SPIP', # MODIF
	'nom_site_spip_plugincode' => 'Códigos de los Plugins',
	'nom_site_spip_programmer' => 'Programar SPIP', # MODIF
	'nom_site_spip_syntaxe' => 'SPIP Sintaxis', # MODIF
	'nom_site_spip_test' => 'SPIP Test', # MODIF
	'nom_site_spip_trad' => 'Traducir SPIP', # MODIF
	'nom_site_spip_video' => 'Media SPIP', # MODIF

	// S
	'slogan_boussole_spip' => '¿Perdido en la Galaxia SPIP?',
	'slogan_groupe_spip_actualite' => 'Actualidad de SPIP',
	'slogan_groupe_spip_aide' => 'Ayuda e intercambios en torno a SPIP',
	'slogan_groupe_spip_decouverte' => 'Al descubrimiento de SPIP',
	'slogan_groupe_spip_extension' => 'Extensiones y contribuciones a SPIP',
	'slogan_groupe_spip_reference' => 'Referencias SPIP',
	'slogan_site_spip_blog' => 'Software libre y ternura',
	'slogan_site_spip_contrib' => 'Espacio de las contribuciones a SPIP',
	'slogan_site_spip_demo' => 'Probar la última versión estable de SPIP',
	'slogan_site_spip_doc' => 'La documentación del código de SPIP',
	'slogan_site_spip_edgard' => '¡Un golpe de robot y esto se pone en marcha!',
	'slogan_site_spip_forum' => 'El foro de los usuarios de SPIP', # MODIF
	'slogan_site_spip_irc' => 'Venga a charlar al chat de SPIP',
	'slogan_site_spip_net' => 'Documentación oficial y descarga de SPIP',
	'slogan_site_spip_plugin' => 'Anuario de los plugins SPIP',
	'slogan_site_spip_plugincode' => 'La documentación del código de los plugins',
	'slogan_site_spip_programmer' => 'Documentación de los desarrolladores SPIP', # MODIF
	'slogan_site_spip_syntaxe' => 'Probar la edición de textos en SPIP',
	'slogan_site_spip_test' => 'Probar la instalación e implementación de un sitio SPIP',
	'slogan_site_spip_trad' => 'Espacio de los traductores de SPIP y de sus contribuciones', # MODIF
	'slogan_site_spip_user' => 'Lista de ayuda entre los usuarios de SPIP', # MODIF
	'slogan_site_spip_video' => 'La mediateca de SPIP',
	'slogan_site_spip_zone' => 'Espacio de desarrollo de las contribuciones a SPIP',
];

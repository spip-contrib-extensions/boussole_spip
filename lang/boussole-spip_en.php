<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// D
	'descriptif_boussole_spip' => 'SPIP compass gathers "official" websites of the SPIP galaxy. For each website, logo, name, slogan and description are defined. Do not hesitate to use this compass on your own website for guiding visitors into the SPIP galaxy.',
	'descriptif_site_spip_blog' => 'SPIP is a collaborative project, so SPIP-BLOG.net  gathers technical notes and announcements as well as self-mockeries, trolls and humorous articles. In this regard, it reflects the SPIP community spirit: tenderness first!.',
	'descriptif_site_spip_contrib' => 'contrib.spip.net is a collaborative website where external contributions are published: plugins, scripts, functions, templates, documentations, tip and tricks... are given to the community (e.g. download links) by SPIP users. Forums preserve relationship between users and developers.',
	'descriptif_site_spip_demo' => 'DEMO.SPIP.net is a test website reset every night. In one click and without having to install it, you can test, as administrator or editor, the SPIP latest stable release.', # MODIF
	'descriptif_site_spip_discuter' => 'DISCUTER.SPIP.net is the new site for exchanges and mutual aid between SPIP users. It brings together all the former discussion lists and the former SPIP forum.',
	'descriptif_site_spip_doc' => 'CODE.SPIP.net is a place where every API, fonction, constant, etc from the SPIP source code is detailed, analysed and comment (name, parameters, description and usage).',
	'descriptif_site_spip_edgard' => 'Edgard is the faithfull and tireless companion of SPIP IRC where he shares his knowledge, with tenderness and humor. From his home, EDGARD.SPIP.net, he brings advices on IRC, answers and happiness. Edgard is definitely a robot, but he doesn’t know about it...',
	'descriptif_site_spip_forge' => 'The GIT.SPIP.net forge is the development space for SPIP and its plugins. It is open by registration, which is carried out using a form on SPIP-Contrib. ', # RELIRE
	'descriptif_site_spip_forum' => 'FORUM.SPIP.net is the place to exchange between SPIP users and get help. This site, translated in about 10 languages, is organised in four main sections: installation and update, private area usage, website administration, management and configuration, templates creation.',
	'descriptif_site_spip_irc' => 'SPIP community never sleeps! A SPIP IRC channel opened 24/7 to everyone: http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_net' => 'SPIP.net is the official website for users and webmasters willing to install a SPIP website, to understand SPIP language (loops, markers and filters), to write and use SPIP templates. Glossary, tutorials, advices and a downloading area of SPIP old versions are available. SPIP.net is translated in more than 20 languages.',
	'descriptif_site_spip_plugin' => 'PLUGINS.SPIP.net is a complete directory of add-ons SPIP (plugins, skeletons, themes). For each add-on are indicated: description, author, licence, compatibility per SPIP version, last updates, status of translations, statistics, documentation and download links.',
	'descriptif_site_spip_plugincode' => 'CODE.PLUGINS.SPIP.net is a SPIP plugins documentation space for their APIs, their source code , and some of their technical behaviours. This website is automatically generated from the PHPDoc included in the code of the plugins.',
	'descriptif_site_spip_programmer' => 'PROGRAMMER.SPIP.net is for developpers or webmasters skilled in PHP, SQL, HTML, CSS and JavaScript. PROGRAMMER.SPIP.net shows most of SPIP technical features (API, overloading, pipeline,...) with numerous coding examples. You can download the entire website in pdf format under free licence cc-by-sa. PROGRAMMER.SPIP.net is translated in French, English and Spanish.',
	'descriptif_site_spip_syntaxe' => 'SPIP Syntax provides a single page with the SPIP publishing form in free access to test all typographical shortcuts and immediately view rendering.',
	'descriptif_site_spip_trad' => 'The translator space welcome those who want to help the SPIP community by participating to translation work of SPIP itself and its miscellaneous contributions',
	'descriptif_site_spip_video' => 'The MEDIAS.SPIP site is an entry point for the distribution of videos made for or about SPIP. Anyone can make a contribution by proposing new video material relating to tutorials, conferences, training courses, etc. The only constraint is to share royalty-free videos so that everyone can consult and use them freely.',

	// N
	'nom_boussole_spip' => 'SPIP Compass',
	'nom_groupe_spip_actualite' => 'News',
	'nom_groupe_spip_aide' => 'Support',
	'nom_groupe_spip_decouverte' => 'Discovery',
	'nom_groupe_spip_extension' => 'Contributions', # MODIF
	'nom_groupe_spip_reference' => 'Documentation',
	'nom_site_spip_blog' => 'SPIP Blog', # MODIF
	'nom_site_spip_contrib' => 'SPIP-Contrib', # MODIF
	'nom_site_spip_demo' => 'SPIP Demo', # MODIF
	'nom_site_spip_discuter' => 'Discussing SPIP ', # MODIF
	'nom_site_spip_doc' => 'SPIP Code', # MODIF
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forge' => 'SPIP Forge ', # MODIF
	'nom_site_spip_forum' => 'SPIP Forums',
	'nom_site_spip_irc' => 'SPIP IRC', # MODIF
	'nom_site_spip_net' => 'SPIP.net', # MODIF
	'nom_site_spip_plugin' => 'SPIP Plugins', # MODIF
	'nom_site_spip_plugincode' => 'Plugins code',
	'nom_site_spip_programmer' => 'Programming with SPIP', # MODIF
	'nom_site_spip_syntaxe' => 'SPIP Syntax', # MODIF
	'nom_site_spip_test' => 'SPIP Test', # MODIF
	'nom_site_spip_trad' => 'Translate SPIP', # MODIF
	'nom_site_spip_video' => 'SPIP Media', # MODIF

	// S
	'slogan_boussole_spip' => 'Lost in SPIP Galaxy?',
	'slogan_groupe_spip_actualite' => ' 	SPIP News',
	'slogan_groupe_spip_aide' => 'SPIP help and exchange',
	'slogan_groupe_spip_decouverte' => ' 	SPIP discovery',
	'slogan_groupe_spip_extension' => ' 	SPIP add-ons and contributions',
	'slogan_groupe_spip_reference' => ' SPIP reference documentation',
	'slogan_site_spip_blog' => 'Free software and tenderness',
	'slogan_site_spip_contrib' => 'SPIP contribution area',
	'slogan_site_spip_demo' => 'Test the last SPIP stable version',
	'slogan_site_spip_discuter' => 'The new SPIP community forums ',
	'slogan_site_spip_doc' => 'Technical coding reference of SPIP',
	'slogan_site_spip_edgard' => 'Let it bot!',
	'slogan_site_spip_forge' => 'The development area for SPIP and its plugins',
	'slogan_site_spip_forum' => 'The SPIP community forum',
	'slogan_site_spip_irc' => 'Welcome to the SPIP chat room',
	'slogan_site_spip_net' => 'Official SPIP documentation and download ',
	'slogan_site_spip_plugin' => 'SPIP Plugins Directory',
	'slogan_site_spip_plugincode' => 'Documentation of the plugin code',
	'slogan_site_spip_programmer' => 'Documentation for developing with SPIP',
	'slogan_site_spip_syntaxe' => 'Test text editing in SPIP',
	'slogan_site_spip_test' => 'Test the set up and the implementation of a SPIP site',
	'slogan_site_spip_trad' => 'Translation area for SPIP and its contributions',
	'slogan_site_spip_user' => 'The SPIP community support list',
	'slogan_site_spip_video' => 'SPIP media library',
	'slogan_site_spip_zone' => 'Area dedicated to SPIP contribution developement',
];

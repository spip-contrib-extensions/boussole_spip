<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bouspip?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// B
	'bouspip_description' => 'Ce plugin fournit le nom, l’url et la description de tous les sites de la Galaxie SPIP. Il doit être utilisé avec le plugin Boussole en mode serveur afin de fournir, via un service web, ses informations à tous les sites souhaitant afficher la boussole SPIP.',
	'bouspip_nom' => 'Boussole SPIP',
	'bouspip_slogan' => 'Les bonnes adresses de la galaxie SPIP !',
];

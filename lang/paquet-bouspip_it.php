<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bouspip?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// B
	'bouspip_description' => 'Ce plugin fournit le nom, l’url et la description de tous les sites de la Galaxie SPIP. Il doit être utilisé avec le plugin Boussole en mode serveur afin de fournir, via un service web, ses informations à tous les sites souhaitant afficher la boussole SPIP.

Questo plugin fornisce il nome, l’URL e la descrizione di tutti i siti della galassia SPIP. Deve essere utilizzato con il plugin Bussola in modalità server per fornire, tramite un servizio web, le sue informazioni a tutti i siti che desiderano visualizzare la bussola SPIP.',
	'bouspip_nom' => 'Bussola SPIP',
	'bouspip_slogan' => 'I migliori indirizzi nella galassia SPIP!',
];

# Boussole de SPIP

Les bonnes adresses de la galaxie SPIP ou d�ailleurs !

Installez ce plugin sur votre site et vous aurez acc�s, dans vos squelettes, � l�ensemble des sites de la Galaxie SPIP en utilisant les mod�les ou noisettes propos�s.

Pour les bricoleurs, vous pouvez aussi utiliser ce plugin pour afficher votre propre liste de sites et concevoir vos propres affichages.


**Documentation officielle**\
https://contrib.spip.net/Boussole